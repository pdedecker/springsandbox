package com.example.sandbox.resources;

import java.time.ZonedDateTime;

/**
 * Holds info about the current instance of the sandbox app.
 */
public class SandboxInfo {
    private ZonedDateTime timeOfStartup;

    public SandboxInfo(ZonedDateTime timeOfStartup) {
        this.timeOfStartup = timeOfStartup;
    }

    public ZonedDateTime getTimeOfStartup() {
        return timeOfStartup;
    }
}
