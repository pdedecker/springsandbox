package com.example.sandbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.ZonedDateTime;

@SpringBootApplication
public class SandboxApplication {
	public static ZonedDateTime timeOfStartup = ZonedDateTime.now();

	public static void main(String[] args) {
		SpringApplication.run(SandboxApplication.class, args);
	}

}
