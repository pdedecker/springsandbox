package com.example.sandbox.controllers;

import com.example.sandbox.SandboxApplication;
import com.example.sandbox.resources.SandboxInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {
    /**
     * Report some basic application info.
     */
    @RequestMapping("/")
    public SandboxInfo info() {
        return new SandboxInfo(SandboxApplication.timeOfStartup);
    }
}
